//
//  ViewController.swift
//  Calculator
//
//  Created by David Thevenin on 25/01/16.
//  Copyright © 2016 David Thevenin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var display: UILabel!
  
  @IBOutlet weak var stack: UILabel!
  
  var userUserInTheMiddleOfTypingANumber: Bool = false
  
  var brain = CalculatorBrain ()
  
  @IBAction func appendDigit(sender: UIButton) {
    let digit = sender.currentTitle!
    
    if userUserInTheMiddleOfTypingANumber {
      if digit == "." && display.text!.rangeOfString(".") != nil { return }
      display.text = display.text! + digit
    }
    else {
      display.text = digit
      userUserInTheMiddleOfTypingANumber = true
    }
  }
  
  @IBAction func appendConst(sender: UIButton) {
    if let operation = sender.currentTitle {
      switch operation {
      case "π":
        displayValue = M_PI
        enter ()
      default: break
      }
    }
  }
  
  @IBAction func operate(sender: UIButton) {
    
    if userUserInTheMiddleOfTypingANumber {
      enter ()
    }
    if let operation = sender.currentTitle {
      if let result = brain.performOperation (operation) {
        displayValue = result;
      } else {
        displayValue = 0
      }
    }
    
//    switch operation {
//    case "C":
//      operandStack.removeAll()
//      display.text = " "
//      stack.text = " "
//      userUserInTheMiddleOfTypingANumber = false
//    default: break
//    }
  }
  
  @IBAction func enter() {
    userUserInTheMiddleOfTypingANumber = false;
    if let result = brain.pushOperand (displayValue) {
      displayValue = result;
    } else {
      displayValue = 0
    }
    
    stack.text = stack.text! + "\n\(displayValue)"
  }
  
  var displayValue: Double {
    get {
      return NSNumberFormatter().numberFromString(display.text!)!.doubleValue
    }
    set {
      display.text = "\(newValue)"
      userUserInTheMiddleOfTypingANumber = false;
    }
  }
}

