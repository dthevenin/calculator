//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by David Thevenin on 28/01/16.
//  Copyright © 2016 David Thevenin. All rights reserved.
//

import Foundation

class CalculatorBrain
{
  private enum Op: CustomStringConvertible {
    case Operand(Double)
    case UnaryOperation (String, Double -> Double)
    case BinaryOperation (String, (Double, Double) -> Double)
    
    var description: String {
      get {
        switch self {
        case .Operand (let operand):
          return "\(operand)"
          
        case .UnaryOperation (let symbol, _):
          return symbol
          
        case .BinaryOperation (let symbol, _):
          return symbol
        }
      }
    }
  }
  
  private var opStack = [Op]()

  private var knowOps = [String:Op]()
  
  init () {
    func learnOp (op: Op) {
      knowOps [op.description] = op
    }
    learnOp (Op.BinaryOperation ("×", *))
    learnOp (Op.BinaryOperation ("−") { $1 - $0 })
    learnOp (Op.BinaryOperation ("+", +))
    learnOp (Op.BinaryOperation ("÷") { $1 / $0 })
    learnOp (Op.UnaryOperation ("√", sqrt))
    learnOp (Op.UnaryOperation ("Sin", sin))
    learnOp (Op.UnaryOperation ("Cos", cos))
  }
  
  private func evaluate (ops: [Op]) -> (result: Double?, remainingOps: [Op]) {
    
    if !ops.isEmpty {
      var remainingOps = ops;
      let op = remainingOps.removeLast ()
      switch op {
      case .Operand (let operand):
        return (operand, remainingOps)
        
      case .UnaryOperation (_, let operation):
        let operandEvaluation = evaluate (remainingOps)
        if let operand = operandEvaluation.result {
          return (operation (operand), operandEvaluation.remainingOps)
        }
        
      case .BinaryOperation (_, let operation):
        let operand1Evaluation = evaluate (remainingOps)
        if let operand1 = operand1Evaluation.result {
          let operand2Evaluation = evaluate (operand1Evaluation.remainingOps)
          if let operand2 = operand2Evaluation.result {
            return (operation (operand1, operand2), operand2Evaluation.remainingOps)
          }
        }
      }
    }
    return (nil, ops)
  }
  
  func evaluate () -> Double? {
    
    let (result, remainingOps) = evaluate (opStack);
    
    print ("\(opStack) = \(result!) with \(remainingOps)")
    
    return result;
  }
  
  func pushOperand (operand: Double) -> Double? {
    
    opStack.append(Op.Operand(operand))
    
    return evaluate ()
  }
  
  func performOperation (symbol: String) -> Double? {
    
    if let operation = knowOps [symbol] {
      opStack.append (operation)
    }
    
    return evaluate ()
  }
}